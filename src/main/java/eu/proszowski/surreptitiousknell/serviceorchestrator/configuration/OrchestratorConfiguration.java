package eu.proszowski.surreptitiousknell.serviceorchestrator.configuration;

import eu.proszowski.surreptitiousknell.serviceorchestrator.*;
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.KnownWords;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrchestratorConfiguration {
    @Bean
    ServiceOrchestratorFacade serviceOrchestratorFacade(){
        return ServiceOrchestratorFacade.builder()
                .fileChecker(new FileChecker() { })
                .idiomsFinder(new IdiomsFinder() { })
                .idiomsClassifier(new IdiomsClassifier() { })
                .usersService(new UsersService() { })
                .imageToTextConverter(new ImageToTextConverter() { })
                .wordsClassifier(userId -> KnownWords.builder().build() )
                .wordsFinder( new WordsFinder() {} )
                .build();
    }
}
