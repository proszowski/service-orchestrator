package eu.proszowski.surreptitiousknell.serviceorchestrator;

import eu.proszowski.surreptitiousknell.serviceorchestrator.model.KnownWords;
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.UserId;

public interface WordsClassifier {
    KnownWords getKnownWordsForUserWithId(UserId userId);
}
