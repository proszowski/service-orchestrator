package eu.proszowski.surreptitiousknell.serviceorchestrator.api;

import eu.proszowski.surreptitiousknell.serviceorchestrator.ServiceOrchestratorFacade;
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.StateOfWords;
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.UserId;
import org.springframework.stereotype.Component;

@Component
public class SimpleStateOfWordsEndpoint implements StateofWordsEndpoint {

    private ServiceOrchestratorFacade serviceOrchestratorFacade;

    public SimpleStateOfWordsEndpoint(ServiceOrchestratorFacade serviceOrchestratorFacade) {
        this.serviceOrchestratorFacade = serviceOrchestratorFacade;
    }

    @Override
    public StateOfWords getStateOfWordsForUserWithId(String userId) {
        return serviceOrchestratorFacade.getKnownWordsForUserWithId(UserId.builder().raw(userId).build());
    }
}
