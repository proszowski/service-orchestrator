package eu.proszowski.surreptitiousknell.serviceorchestrator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceOrchestratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceOrchestratorApplication.class, args);
    }

}
