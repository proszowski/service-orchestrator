package eu.proszowski.surreptitiousknell.serviceorchestrator;

public class UserIdNotFoundException extends RuntimeException {
    private UserIdNotFoundException(String messsage) {
        super(messsage);
    }

    public static UserIdNotFoundException withMessage(String messsage) {
        return new UserIdNotFoundException(messsage);
    }
}
