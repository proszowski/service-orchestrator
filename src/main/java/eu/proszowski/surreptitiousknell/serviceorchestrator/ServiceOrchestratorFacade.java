package eu.proszowski.surreptitiousknell.serviceorchestrator;

import eu.proszowski.surreptitiousknell.serviceorchestrator.model.KnownWords;
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.StateOfWords;
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.UserId;
import lombok.*;

@Builder
public class ServiceOrchestratorFacade {

    @NonNull
    private UsersService usersService;

    @NonNull
    private FileChecker fileChecker;

    @NonNull
    private ImageToTextConverter imageToTextConverter;

    @NonNull
    private IdiomsClassifier idiomsClassifier;

    @NonNull
    private WordsClassifier wordsClassifier;

    @NonNull
    private WordsFinder wordsFinder;

    @NonNull
    private IdiomsFinder idiomsFinder;

    public StateOfWords getKnownWordsForUserWithId(UserId userId) {
        KnownWords knownWords = wordsClassifier.getKnownWordsForUserWithId(userId);
        return StateOfWords.builder().knownWords(knownWords).build();
    }
}
