package eu.proszowski.surreptitiousknell.serviceorchestrator.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Word {
    private String value;
}
