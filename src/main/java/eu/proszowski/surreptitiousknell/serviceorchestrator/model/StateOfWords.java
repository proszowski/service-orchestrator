package eu.proszowski.surreptitiousknell.serviceorchestrator.model;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;


@Value
@Builder
public class StateOfWords {

    @NonNull
    private KnownWords knownWords;

    public KnownWords getKnownWords() {
        return knownWords;
    }
}
