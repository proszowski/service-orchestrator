package eu.proszowski.surreptitiousknell.serviceorchestrator.model;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class KnownWords {
    private List<Word> words;
}
