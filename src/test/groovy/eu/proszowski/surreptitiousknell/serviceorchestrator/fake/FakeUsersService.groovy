package eu.proszowski.surreptitiousknell.serviceorchestrator.fake

import eu.proszowski.surreptitiousknell.serviceorchestrator.model.UserId
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.UserInfo
import eu.proszowski.surreptitiousknell.serviceorchestrator.UsersService
import groovy.transform.builder.Builder

@Builder
class FakeUsersService implements UsersService {

    Map<UserId, UserInfo> users = new HashMap<>()

    static FakeUsersService withAnUser(UserId userId, UserInfo userInfo) {
        return builder().users(Collections.singletonMap(userId, userInfo)).build()
    }

}
