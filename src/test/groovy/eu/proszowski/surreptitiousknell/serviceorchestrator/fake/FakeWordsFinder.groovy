package eu.proszowski.surreptitiousknell.serviceorchestrator.fake

import eu.proszowski.surreptitiousknell.serviceorchestrator.WordsFinder
import groovy.transform.builder.Builder

@Builder
class FakeWordsFinder implements WordsFinder {
}
