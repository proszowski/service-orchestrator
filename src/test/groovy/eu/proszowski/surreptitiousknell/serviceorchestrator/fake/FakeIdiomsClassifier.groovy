package eu.proszowski.surreptitiousknell.serviceorchestrator.fake

import eu.proszowski.surreptitiousknell.serviceorchestrator.IdiomsClassifier
import groovy.transform.builder.Builder

@Builder
class FakeIdiomsClassifier implements IdiomsClassifier {
}
