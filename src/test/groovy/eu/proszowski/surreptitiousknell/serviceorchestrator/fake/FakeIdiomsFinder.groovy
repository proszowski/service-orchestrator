package eu.proszowski.surreptitiousknell.serviceorchestrator.fake

import eu.proszowski.surreptitiousknell.serviceorchestrator.IdiomsFinder
import groovy.transform.builder.Builder

@Builder
class FakeIdiomsFinder implements IdiomsFinder {
}
