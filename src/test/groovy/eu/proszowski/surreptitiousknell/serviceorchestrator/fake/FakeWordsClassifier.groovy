package eu.proszowski.surreptitiousknell.serviceorchestrator.fake

import eu.proszowski.surreptitiousknell.serviceorchestrator.model.KnownWords
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.UserId
import eu.proszowski.surreptitiousknell.serviceorchestrator.UserIdNotFoundException
import eu.proszowski.surreptitiousknell.serviceorchestrator.WordsClassifier
import groovy.transform.builder.Builder

@Builder
class FakeWordsClassifier implements WordsClassifier {

    Map<UserId, KnownWords> knownWordsGroupedByUserId = new HashMap<>();

    static FakeWordsClassifier withAnUserWithGivenIdAndListOfKnownWords(UserId userId, KnownWords knownWords) {
        return builder().knownWordsGroupedByUserId(Collections.singletonMap(userId, knownWords)).build()
    }

    @Override
    KnownWords getKnownWordsForUserWithId(UserId userId) {
        KnownWords knownWords = knownWordsGroupedByUserId.get(userId);
        if (knownWords == null) {
            throw UserIdNotFoundException.withMessage(String.format("User with id %s not found while trying to " +
                    "get known words.", userId.raw));
        } else {
            return knownWords;
        }
    }
}
