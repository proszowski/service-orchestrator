package eu.proszowski.surreptitiousknell.serviceorchestrator.fake

import eu.proszowski.surreptitiousknell.serviceorchestrator.FileChecker
import groovy.transform.builder.Builder

@Builder
class FakeFileChecker implements FileChecker {
}
