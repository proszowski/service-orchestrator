package eu.proszowski.surreptitiousknell.serviceorchestrator.fake

import eu.proszowski.surreptitiousknell.serviceorchestrator.ImageToTextConverter
import groovy.transform.builder.Builder

@Builder
class FakeImageToTextConverter implements ImageToTextConverter {
}
