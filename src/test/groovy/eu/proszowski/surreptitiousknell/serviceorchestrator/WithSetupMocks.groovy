package eu.proszowski.surreptitiousknell.serviceorchestrator

import eu.proszowski.surreptitiousknell.serviceorchestrator.fake.*
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.KnownWords
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.UserId

trait WithSetupMocks implements WithUserInfoMocks {

    FakeUsersService usersService;
    FakeFileChecker fileChecker;
    FakeImageToTextConverter imageToTextConverter;
    FakeIdiomsClassifier idiomsClassifier;
    FakeWordsClassifier wordsClassifier;
    FakeWordsFinder wordsFinder;
    FakeIdiomsFinder idiomsFinder;
    ServiceOrchestratorFacade serviceOrchestratorFacade;

    void thereIsAnUserWithGivenIdAndKnownWords(UserId userId, KnownWords knownWords) {
        Closure setupUsersService = { usersService = FakeUsersService.withAnUser(userId, dummyUserInfo()) }
        Closure setupWordsClassifier = {
            wordsClassifier = FakeWordsClassifier.withAnUserWithGivenIdAndListOfKnownWords(userId, knownWords)
        }
        setupServiceOrchestratorFacade(setupUsersService, setupWordsClassifier)
    }

    void thereIsADefaultSetupOfOrchestrator() {
        setupServiceOrchestratorFacade([] as Closure[]);
    }

    private void setupServiceOrchestratorFacade(Closure... setups) {
        usersService = FakeUsersService.builder().users(Collections.emptyMap()).build();
        fileChecker = FakeFileChecker.builder().build();
        imageToTextConverter = FakeImageToTextConverter.builder().build();
        idiomsClassifier = FakeIdiomsClassifier.builder().build();
        wordsClassifier = FakeWordsClassifier.builder().knownWordsGroupedByUserId(Collections.emptyMap()).build();
        wordsFinder = FakeWordsFinder.builder().build();
        idiomsFinder = FakeIdiomsFinder.builder().build();

        for (setup in setups) {
            setup.run()
        }

        serviceOrchestratorFacade = ServiceOrchestratorFacade.builder()
                .usersService(usersService)
                .fileChecker(fileChecker)
                .imageToTextConverter(imageToTextConverter)
                .idiomsClassifier(idiomsClassifier)
                .wordsClassifier(wordsClassifier)
                .wordsFinder(wordsFinder)
                .idiomsFinder(idiomsFinder)
                .build();
    }
}