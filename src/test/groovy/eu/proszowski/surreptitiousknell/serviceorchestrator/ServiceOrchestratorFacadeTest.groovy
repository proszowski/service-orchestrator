package eu.proszowski.surreptitiousknell.serviceorchestrator

import eu.proszowski.surreptitiousknell.serviceorchestrator.model.KnownWords
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.StateOfWords
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.UserId
import eu.proszowski.surreptitiousknell.serviceorchestrator.model.Word
import org.assertj.core.api.WithAssertions
import spock.lang.Specification

import static org.testng.Assert.assertTrue

class ServiceOrchestratorFacadeTest extends Specification implements WithAssertions, WithSetupMocks {

    void "should return list of known words"() {
        given:
        UserId userId = UserId.builder().raw(UUID.randomUUID().toString()).build();
        List<Word> dummyWords = [Word.builder().value("dummy").build(), Word.builder().value("words").build()];
        KnownWords knownWords = KnownWords.builder().words(dummyWords).build();

        thereIsAnUserWithGivenIdAndKnownWords(userId, knownWords);

        when:
        StateOfWords response = serviceOrchestratorFacade.getKnownWordsForUserWithId(userId);

        then:
        assertTrue(response.getKnownWords().getWords().containsAll(knownWords.getWords()));
    }

    void "should throw exception when given id does not exist while asking about list of known words"() {
        given:
        UserId userId = UserId.builder()
                .raw(UUID.randomUUID().toString())
                .build();
        thereIsADefaultSetupOfOrchestrator()

        when:
        Closure closure = { serviceOrchestratorFacade.getKnownWordsForUserWithId(userId) };

        then:
        assertThatCode({
            closure.run()
        }).isInstanceOf(UserIdNotFoundException)
    }

}
