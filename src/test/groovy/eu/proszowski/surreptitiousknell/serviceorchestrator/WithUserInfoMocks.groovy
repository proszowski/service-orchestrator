package eu.proszowski.surreptitiousknell.serviceorchestrator

import eu.proszowski.surreptitiousknell.serviceorchestrator.model.UserInfo

trait WithUserInfoMocks {

    UserInfo dummyUserInfo() {
        return UserInfo.builder().build();
    }

}